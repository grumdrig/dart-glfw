// Copyright (c) 2012, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

library test_sample_extension;

import 'dart:io';
import 'sample_synchronous_extension.dart';

void check(bool condition, String message) {
  if (!condition) {
    throw new StateError(message);
  }
}

void main() {
  print("Start");
  boop("window");
  stdin.readLineSync();
  return;

  //var window = glfwCreateWindow(500, 500, "Hello Glfw", null, null);
  //boop("window");
  var window = 1;
  print("Window $window");
  //boop("love");
  // stdin.readLineSync();


  while ((window != 0) && !glfwWindowShouldClose(window)) {
    print("poll");
    //glfwPollEvents();
    boop("poll");
  }

  print("End");
}
