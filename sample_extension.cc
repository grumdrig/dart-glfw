// Copyright (c) 2012, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <GLFW/glfw3.h>
#include "include/dart_api.h"
#include "include/dart_native_api.h"

// Re main thread issue:
// https://groups.google.com/a/dartlang.org/forum/#!msg/eng/8LrOzCam1Wo/NnFP3jZ9mIoJ

Dart_Port globalMainPortId;
int hackage = 0;
int64_t globalWindow = 0;

Dart_NativeFunction ResolveName(Dart_Handle name, int argc);

Dart_Handle HandleError(Dart_Handle handle) {
  if (Dart_IsError(handle)) Dart_PropagateError(handle);
  return handle;
}

void GlfwErrorCallback(int error, const char* description) {
  fprintf(stderr, "GLFW Error: %s\n", description);
}

void MessageCallback(Dart_Isolate dest_isolate) {
  Dart_EnterScope();
  /*
  Dart_Handle message = HandleError(Dart_HandleMessage());
  if (Dart_IsString(message)) {
    const char* str;
    HandleError(Dart_StringToCString(message, &str));
    printf("Message received: %s\n", str);
  } else*/ {
  uint64_t tid;
  pthread_threadid_np(NULL, &tid);
  printf("Message port id %lld thread %lld\n", Dart_GetMainPortId(), tid);
  }
  if (hackage++ > 3) {
    printf("...poll\n");
    glfwPollEvents();
  } else {
    printf("...create window\n");
    globalWindow = (int64_t)glfwCreateWindow(300,300,"Hackery",NULL,NULL);
  }
  Dart_ExitScope();
}

void doglfw() {
  glfwInit();
  GLFWwindow* window = glfwCreateWindow(300,300,"Hackery",NULL,NULL);
  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();
  }
  glfwTerminate();
}


void info(const char* context) {

  uint64_t tid;
  pthread_threadid_np(NULL, &tid);
  printf("INFO %s: main port %lld thread %lld isolate %08x\n",
         context, Dart_GetMainPortId(), tid, Dart_CurrentIsolate());
}

DART_EXPORT Dart_Handle sample_extension_Init(Dart_Handle parent_library) {
  if (Dart_IsError(parent_library)) { return parent_library; }

  Dart_Handle result_code = Dart_SetNativeResolver(parent_library, ResolveName);
  if (Dart_IsError(result_code)) return result_code;

  // glfwSetErrorCallback(GlfwErrorCallback);

  // if (!glfwInit())
    // return Dart_NewApiError("Error initializing GLFW");

  // atexit(glfwTerminate);


  // Dart_SetMessageNotifyCallback(MessageCallback);

  globalMainPortId = Dart_GetMainPortId();
  info("Init");


  doglfw();

  return Dart_Null();
}

void Boop(Dart_NativeArguments arguments) {
  Dart_EnterScope();
  //Dart_Handle arg = HandleError(Dart_GetNativeArgument(arguments, 0));
  info("boop");
  // Dart_Post(globalMainPortId, Dart_Null());
  // doglfw();
  Dart_ExitScope();
}

void GlfwCreateWindow(Dart_NativeArguments arguments) {
  // GLFWwindow* glfwCreateWindow(int width, int height, const char* title,
  //                              GLFWmonitor* monitor, GLFWwindow* share);
  Dart_EnterScope();
  GLFWwindow* window = NULL;
  Dart_Handle args[5];
  for (int i = 0; i < 5; ++i) args[i] = HandleError(Dart_GetNativeArgument(arguments, i));
  if (Dart_IsInteger(args[0]) &&
      Dart_IsInteger(args[1]) &&
      Dart_IsString(args[2]) &&
      (Dart_IsInteger(args[3]) || Dart_IsNull(args[3])) &&
      (Dart_IsInteger(args[4]) || Dart_IsNull(args[4]))) {
    //bool fits; HandleError(Dart_IntegerFitsIntoInt64(seed_object, &fits)); if (fits)
    int64_t width, height, monitor = 0, share = 0;
    const char* title;
    HandleError(Dart_IntegerToInt64(args[0], &width));
    HandleError(Dart_IntegerToInt64(args[1], &height));
    HandleError(Dart_StringToCString(args[2], &title));
    if (!Dart_IsNull(args[3]))
      HandleError(Dart_IntegerToInt64(args[3], &monitor));
    if (!Dart_IsNull(args[4]))
      HandleError(Dart_IntegerToInt64(args[4], &share));
    window = glfwCreateWindow((int)width, (int)height, title, (GLFWmonitor*)monitor, (GLFWwindow*)share);
    /*
    This works, since it happens in the main thread. That's small consolation, of course.
    printf("Window: %d\n", window);
    while ((window != 0) && !glfwWindowShouldClose(window)) {
      printf("poll:\n");
      glfwPollEvents();
    }
    */
  }
  Dart_SetReturnValue(arguments, HandleError(Dart_NewInteger(int64_t(window))));
  Dart_ExitScope();
}

void GlfwWindowShouldClose(Dart_NativeArguments arguments) {
  Dart_EnterScope();
  Dart_Handle hwindow = HandleError(Dart_GetNativeArgument(arguments, 0));
  printf("Should close?\n");
  if (Dart_IsInteger(hwindow)) {
    int64_t window;
    HandleError(Dart_IntegerToInt64(hwindow, &window));
    if (globalWindow) window = globalWindow;
    if(globalWindow)
    printf("Should close? %lld %d\n", window, glfwWindowShouldClose((GLFWwindow*)window));
    Dart_SetReturnValue(arguments, HandleError(Dart_NewBoolean(glfwWindowShouldClose((GLFWwindow*)window))));
  } else {
    printf("Should close? notint\n");
    Dart_SetReturnValue(arguments, HandleError(Dart_NewApiError("Expected window handle")));
  }
  Dart_ExitScope();
}

void GlfwPollEvents(Dart_NativeArguments arguments) {
  Dart_EnterScope();
  glfwPollEvents();
  printf("pole\n");
  Dart_ExitScope();
}


/*
uint8_t* randomArray(int seed, int length) {
  if (length <= 0 || length > 10000000) return NULL;
  uint8_t* values = reinterpret_cast<uint8_t*>(malloc(length));
  if (NULL == values) return NULL;
  srand(seed);
  for (int i = 0; i < length; ++i) {
    values[i] = rand() % 256;
  }
  return values;
}

void wrappedRandomArray(Dart_Port dest_port_id,
                        Dart_CObject* message) {
  Dart_Port reply_port_id = ILLEGAL_PORT;
  if (message->type == Dart_CObject_kArray &&
      3 == message->value.as_array.length) {
    // Use .as_array and .as_int32 to access the data in the Dart_CObject.
    Dart_CObject* param0 = message->value.as_array.values[0];
    Dart_CObject* param1 = message->value.as_array.values[1];
    Dart_CObject* param2 = message->value.as_array.values[2];
    if (param0->type == Dart_CObject_kInt32 &&
        param1->type == Dart_CObject_kInt32 &&
        param2->type == Dart_CObject_kSendPort) {
      int seed = param0->value.as_int32;
      int length = param1->value.as_int32;
      reply_port_id = param2->value.as_send_port;
      uint8_t* values = randomArray(seed, length);

      if (values != NULL) {
        Dart_CObject result;
        result.type = Dart_CObject_kTypedData;
        result.value.as_typed_data.type = Dart_TypedData_kUint8;
        result.value.as_typed_data.values = values;
        result.value.as_typed_data.length = length;
        Dart_PostCObject(reply_port_id, &result);
        free(values);
        // It is OK that result is destroyed when function exits.
        // Dart_PostCObject has copied its data.
        return;
      }
    }
  }
  Dart_CObject result;
  result.type = Dart_CObject_kNull;
  Dart_PostCObject(reply_port_id, &result);
}

void randomArrayServicePort(Dart_NativeArguments arguments) {
  Dart_EnterScope();
  Dart_SetReturnValue(arguments, Dart_Null());
  Dart_Port service_port =
      Dart_NewNativePort("RandomArrayService", wrappedRandomArray, true);
  if (service_port != ILLEGAL_PORT) {
    Dart_Handle send_port = HandleError(Dart_NewSendPort(service_port));
    Dart_SetReturnValue(arguments, send_port);
  }
  Dart_ExitScope();
}
*/

struct FunctionLookup {
  const char* name;
  Dart_NativeFunction function;
};

FunctionLookup function_list[] = {
  {"GlfwCreateWindow", GlfwCreateWindow},
  {"GlfwWindowShouldClose", GlfwWindowShouldClose},
  {"GlfwPollEvents", GlfwPollEvents},
  {"Boop", Boop},
  // {"RandomArray_ServicePort", randomArrayServicePort},
  {NULL, NULL}};

Dart_NativeFunction ResolveName(Dart_Handle name, int argc) {
  if (!Dart_IsString(name)) return NULL;
  Dart_NativeFunction result = NULL;
  Dart_EnterScope();
  const char* cname;
  HandleError(Dart_StringToCString(name, &cname));

  for (int i=0; function_list[i].name != NULL; ++i) {
    if (strcmp(function_list[i].name, cname) == 0) {
      result = function_list[i].function;
      break;
    }
  }
  Dart_ExitScope();
  return result;
}
