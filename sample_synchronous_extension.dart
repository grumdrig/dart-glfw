// Copyright (c) 2012, the Dart project authors.  Please see the AUTHORS file
// for details. All rights reserved. Use of this source code is governed by a
// BSD-style license that can be found in the LICENSE file.

library sample_synchronous_extension;

import 'dart-ext:sample_extension';

// The simplest way to call native code: top-level functions.
int glfwCreateWindow(int width, int height, string title, int monitor, int share) native "GlfwCreateWindow";
bool glfwWindowShouldClose(int window) native "GlfwWindowShouldClose";
void glfwPollEvents() native "GlfwPollEvents";
void boop(string message) native "Boop";
int systemRand() native "SystemRand";
bool systemSrand(int seed) native "SystemSrand";
