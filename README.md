dart-glfw
=========

Native extension to GLFW3 for Dart
----------------------------------

The project is on ice since I'm not sure how to move forward. Various of the
GLFW calls need to be made from the main thread. Though the main dart program
runs in the same isolate, apparently, as is current when the init function is
called, the thread ID changes, and the GLFW calls wont work on the new thread.

Links for reference
-------------------

### All out of date info for dart embedding:

* http://www.marshut.com/ixpki/embedding-the-dart-vm-in-a-standalone-project.html

* https://github.com/donny-dont/DartEmbeddingDemo

* https://groups.google.com/a/dartlang.org/forum/?fromgroups#!searchin/misc/embed/misc/qBuKmUBltas/oCclEA5DJYMJ

### More recent but not sure of build process:

* https://github.com/nelsonsilva/openglui
